import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
## @type: DataSource
## @args: [database = "phoenixdb", table_name = "mydemoobuckett31", transformation_ctx = "datasource0"]
## @return: datasource0
## @inputs: []
datasource0 = glueContext.create_dynamic_frame.from_catalog(database = "phoenixdb", table_name = "mydemoobuckett31", transformation_ctx = "datasource0")
## @type: ApplyMapping
## @args: [mapping = [("temperaturemax", "double", "temperaturemax", "double"), ("temperaturemaxtime", "string", "temperaturemaxtime", "string"), ("windbearing", "long", "windbearing", "long"), ("icon", "string", "icon", "string"), ("dewpoint", "double", "dewpoint", "double"), ("temperaturemintime", "string", "temperaturemintime", "string"), ("cloudcover", "double", "cloudcover", "double"), ("windspeed", "double", "windspeed", "double"), ("pressure", "double", "pressure", "double"), ("apparenttemperaturemintime", "string", "apparenttemperaturemintime", "string"), ("apparenttemperaturehigh", "double", "apparenttemperaturehigh", "double"), ("preciptype", "string", "preciptype", "string"), ("visibility", "double", "visibility", "double"), ("humidity", "double", "humidity", "double"), ("apparenttemperaturehightime", "string", "apparenttemperaturehightime", "string"), ("apparenttemperaturelow", "double", "apparenttemperaturelow", "double"), ("apparenttemperaturemax", "double", "apparenttemperaturemax", "double"), ("uvindex", "double", "uvindex", "double"), ("time", "string", "time", "string"), ("sunsettime", "string", "sunsettime", "string"), ("temperaturelow", "double", "temperaturelow", "double"), ("temperaturemin", "double", "temperaturemin", "double"), ("temperaturehigh", "double", "temperaturehigh", "double"), ("sunrisetime", "string", "sunrisetime", "string"), ("temperaturehightime", "string", "temperaturehightime", "string"), ("uvindextime", "string", "uvindextime", "string"), ("summary", "string", "summary", "string"), ("temperaturelowtime", "string", "temperaturelowtime", "string"), ("apparenttemperaturemin", "double", "apparenttemperaturemin", "double"), ("apparenttemperaturemaxtime", "string", "apparenttemperaturemaxtime", "string"), ("apparenttemperaturelowtime", "string", "apparenttemperaturelowtime", "string"), ("moonphase", "double", "moonphase", "double")], transformation_ctx = "applymapping1"]
## @return: applymapping1
## @inputs: [frame = datasource0]
applymapping1 = ApplyMapping.apply(frame = datasource0, mappings = [("temperaturemax", "double", "temperaturemax", "double"), ("temperaturemaxtime", "string", "temperaturemaxtime", "string"), ("windbearing", "long", "windbearing", "long"), ("icon", "string", "icon", "string"), ("dewpoint", "double", "dewpoint", "double"), ("temperaturemintime", "string", "temperaturemintime", "string"), ("cloudcover", "double", "cloudcover", "double"), ("windspeed", "double", "windspeed", "double"), ("pressure", "double", "pressure", "double"), ("apparenttemperaturemintime", "string", "apparenttemperaturemintime", "string"), ("apparenttemperaturehigh", "double", "apparenttemperaturehigh", "double"), ("preciptype", "string", "preciptype", "string"), ("visibility", "double", "visibility", "double"), ("humidity", "double", "humidity", "double"), ("apparenttemperaturehightime", "string", "apparenttemperaturehightime", "string"), ("apparenttemperaturelow", "double", "apparenttemperaturelow", "double"), ("apparenttemperaturemax", "double", "apparenttemperaturemax", "double"), ("uvindex", "double", "uvindex", "double"), ("time", "string", "time", "string"), ("sunsettime", "string", "sunsettime", "string"), ("temperaturelow", "double", "temperaturelow", "double"), ("temperaturemin", "double", "temperaturemin", "double"), ("temperaturehigh", "double", "temperaturehigh", "double"), ("sunrisetime", "string", "sunrisetime", "string"), ("temperaturehightime", "string", "temperaturehightime", "string"), ("uvindextime", "string", "uvindextime", "string"), ("summary", "string", "summary", "string"), ("temperaturelowtime", "string", "temperaturelowtime", "string"), ("apparenttemperaturemin", "double", "apparenttemperaturemin", "double"), ("apparenttemperaturemaxtime", "string", "apparenttemperaturemaxtime", "string"), ("apparenttemperaturelowtime", "string", "apparenttemperaturelowtime", "string"), ("moonphase", "double", "moonphase", "double")], transformation_ctx = "applymapping1")
## @type: SelectFields
## @args: [paths = ["temperaturemax", "temperaturemaxtime", "windbearing", "icon", "dewpoint", "temperaturemintime", "cloudcover", "windspeed", "pressure", "apparenttemperaturemintime", "apparenttemperaturehigh", "preciptype", "visibility", "humidity", "apparenttemperaturehightime", "apparenttemperaturelow", "apparenttemperaturemax", "uvindex", "time", "sunsettime", "temperaturelow", "temperaturemin", "temperaturehigh", "sunrisetime", "temperaturehightime", "uvindextime", "summary", "temperaturelowtime", "apparenttemperaturemin", "apparenttemperaturemaxtime", "apparenttemperaturelowtime", "moonphase"], transformation_ctx = "selectfields2"]
## @return: selectfields2
## @inputs: [frame = applymapping1]
selectfields2 = SelectFields.apply(frame = applymapping1, paths = ["temperaturemax", "temperaturemaxtime", "windbearing", "icon", "dewpoint", "temperaturemintime", "cloudcover", "windspeed", "pressure", "apparenttemperaturemintime", "apparenttemperaturehigh", "preciptype", "visibility", "humidity", "apparenttemperaturehightime", "apparenttemperaturelow", "apparenttemperaturemax", "uvindex", "time", "sunsettime", "temperaturelow", "temperaturemin", "temperaturehigh", "sunrisetime", "temperaturehightime", "uvindextime", "summary", "temperaturelowtime", "apparenttemperaturemin", "apparenttemperaturemaxtime", "apparenttemperaturelowtime", "moonphase"], transformation_ctx = "selectfields2")
## @type: ResolveChoice
## @args: [choice = "MATCH_CATALOG", database = "phoenixdb", table_name = "mydemoobuckett31", transformation_ctx = "resolvechoice3"]
## @return: resolvechoice3
## @inputs: [frame = selectfields2]
resolvechoice3 = ResolveChoice.apply(frame = selectfields2, choice = "MATCH_CATALOG", database = "phoenixdb", table_name = "mydemoobuckett31", transformation_ctx = "resolvechoice3")
## @type: DataSink
## @args: [database = "phoenixdb", table_name = "mydemoobuckett31", transformation_ctx = "datasink4"]
## @return: output
## @inputs: [frame = resolvechoice3]
output = glueContext.write_dynamic_frame.from_options(frame = resolvechoice3, connection_type = "s3", connection_options = {"path":"s3://phoenixoutput"}, format = "csv", transformation_ctx = "output")

job.commit()